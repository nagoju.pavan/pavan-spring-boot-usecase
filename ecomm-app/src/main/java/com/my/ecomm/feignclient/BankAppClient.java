package com.my.ecomm.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "http://BANK-TRANSFER-APP/bank")
public interface BankAppClient {

	@PostMapping("/fund-transfer")
	public ResponseEntity<?> bankTransfer(@RequestBody PaymentRequest payment);

}
