package com.my.ecomm.feignclient;

import java.io.Serializable;

public class PaymentRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double payAmount;

	private String fromAccount;

	private String toAccount;

	public PaymentRequest(Double payAmount, String fromAccount, String toAccount) {
		super();
		this.payAmount = payAmount;
		this.fromAccount = fromAccount;
		this.toAccount = toAccount;
	}

	public Double getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(Double payAmount) {
		this.payAmount = payAmount;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public String getToAccount() {
		return toAccount;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

}