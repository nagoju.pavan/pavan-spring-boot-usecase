package com.my.ecomm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.my.ecomm.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/search")
	public ResponseEntity<?> searchProduct(@RequestParam(required = false) String productName,
			@RequestParam(required = false) String categoryName) {
		return productService.getProductByProductnameOrCategoryName(productName, categoryName);
	}

}
