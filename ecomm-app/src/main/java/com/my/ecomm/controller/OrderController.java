package com.my.ecomm.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.my.ecomm.dto.PurchaseOrderRequest;
import com.my.ecomm.service.OrderService;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@PostMapping("/place-order")
	public ResponseEntity<?> placeOrder(@Valid @RequestBody PurchaseOrderRequest orderRequest) {
		return orderService.placeOrder(orderRequest);
	}

	@GetMapping("/history/{customerId}")
	public ResponseEntity<?> orderHistory(@Valid @PathVariable(required = true) Long customerId) {
		return orderService.orderHistory(customerId);
	}

}
