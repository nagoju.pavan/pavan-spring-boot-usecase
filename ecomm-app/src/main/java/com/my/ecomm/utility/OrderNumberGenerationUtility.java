package com.my.ecomm.utility;

import org.apache.commons.lang3.RandomStringUtils;

public class OrderNumberGenerationUtility {

	public static String generateCustomerOrderNumber() {
		return RandomStringUtils.randomNumeric(20).toString();

	}

}
