package com.my.ecomm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.my.ecomm.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
