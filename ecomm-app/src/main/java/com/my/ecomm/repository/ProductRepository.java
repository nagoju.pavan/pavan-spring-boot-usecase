package com.my.ecomm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.ecomm.entity.ProductEntity;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

	List<ProductEntity> findByProductNameIsContainingIgnoreCaseAndProductCategoryCategoryNameIsContainingIgnoreCase(
			final String productName, final String categoryName);

	List<ProductEntity> findByProductNameIsContainingIgnoreCase(final String productName);

	List<ProductEntity> findByProductCategoryCategoryNameIsContainingIgnoreCase(final String categoryName);

}
