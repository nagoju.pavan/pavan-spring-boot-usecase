package com.my.ecomm.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.my.ecomm.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

	List<Order> findByCustomerCustomerId(Long customerId);
}
