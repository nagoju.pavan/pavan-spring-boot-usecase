package com.my.ecomm.service;

import org.springframework.http.ResponseEntity;

import com.my.ecomm.dto.PurchaseOrderRequest;

public interface OrderService {

	ResponseEntity<?> placeOrder(PurchaseOrderRequest orderRequest);

	ResponseEntity<?> orderHistory(Long customerId);
}
