package com.my.ecomm.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.my.ecomm.entity.ProductEntity;
import com.my.ecomm.repository.ProductRepository;
import com.my.ecomm.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Override
	public ResponseEntity<?> getProductByProductnameOrCategoryName(final String productName,
			final String categoryName) {
		List<ProductEntity> searchResponse = new ArrayList<>();
		if (StringUtils.isBlank(categoryName) && StringUtils.isBlank(productName)) {
			return new ResponseEntity<>("Invalid Search Critiria", HttpStatus.BAD_REQUEST);
		} else {
			if (StringUtils.isNotBlank(categoryName) && StringUtils.isNotBlank(productName)) {
				searchResponse = productRepository
						.findByProductNameIsContainingIgnoreCaseAndProductCategoryCategoryNameIsContainingIgnoreCase(
								productName, categoryName);
			} else if (StringUtils.isNotBlank(categoryName)) {
				searchResponse = productRepository
						.findByProductCategoryCategoryNameIsContainingIgnoreCase(categoryName);
			} else {
				searchResponse = productRepository.findByProductNameIsContainingIgnoreCase(productName);
			}
			return new ResponseEntity<>(searchResponse, HttpStatus.OK);
		}
	}

}
