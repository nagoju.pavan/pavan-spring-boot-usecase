package com.my.ecomm.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.my.ecomm.dto.OrderDto;
import com.my.ecomm.dto.OrderHistory;
import com.my.ecomm.dto.ProductItems;
import com.my.ecomm.dto.ProductOrderDetails;
import com.my.ecomm.dto.PurchaseOrderRequest;
import com.my.ecomm.entity.Customer;
import com.my.ecomm.entity.Order;
import com.my.ecomm.entity.OrderDetails;
import com.my.ecomm.entity.ProductEntity;
import com.my.ecomm.feignclient.BankAppClient;
import com.my.ecomm.feignclient.PaymentRequest;
import com.my.ecomm.repository.CustomerRepository;
import com.my.ecomm.repository.OrderDetailsRepository;
import com.my.ecomm.repository.OrderRepository;
import com.my.ecomm.repository.ProductRepository;
import com.my.ecomm.service.OrderService;
import com.my.ecomm.utility.OrderNumberGenerationUtility;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	OrderDetailsRepository orderDetailsRepository;

	@Autowired
	BankAppClient bankAppClient;

	private String systemAccountNumber = "9494999999";

	@Override
	public ResponseEntity<?> placeOrder(final PurchaseOrderRequest orderRequest) {
		Set<Long> productIds = new HashSet<>();
		for (ProductItems productItem : orderRequest.getProductItems()) {
			productIds.add(productItem.getProductId());
		}
		List<ProductEntity> products = (List<ProductEntity>) productRepository.findAllById(productIds);
		for (ProductItems item : orderRequest.getProductItems()) {
			if (item.getQuantity() == null) {
				return new ResponseEntity<>("Invalid Quatity", HttpStatus.BAD_REQUEST);
			}
			if (item.getProductId() == null) {
				return new ResponseEntity<>("Invalid Product", HttpStatus.BAD_REQUEST);
			}
		}
		Optional<Customer> customer = customerRepository.findById(orderRequest.getCustomerId());
		if (!customer.isPresent()) {
			return new ResponseEntity<>("Invalid Customer", HttpStatus.BAD_REQUEST);
		}
		if (products.isEmpty()) {
			return new ResponseEntity<>("Invalid Products", HttpStatus.BAD_REQUEST);
		}
		
		
		Double orderAmount = products.stream().mapToDouble(p -> p.getProductPrice()).sum();
		Order customerOrder = new Order();
		customerOrder.setCustomer(customer.get());
		customerOrder.setOrderDate(new Date());
		customerOrder.setOrderNumber(OrderNumberGenerationUtility.generateCustomerOrderNumber());
		customerOrder.setOrderStatus(2);
		customerOrder.setOrderAmount(orderAmount);
		orderRepository.save(customerOrder);
		List<OrderDetails> orderDetailsList = new ArrayList<>();
		for (ProductItems productItem : orderRequest.getProductItems()) {
			OrderDetails orderDetails = new OrderDetails();
			orderDetails.setOrder(customerOrder);
			ProductEntity productEntity = productRepository.findById(productItem.getProductId()).get();
			orderDetails.setProduct(productEntity);
			orderDetails.setProductQuantity(productItem.getQuantity());
			orderDetails.setProductPrice(productEntity.getProductPrice());
			orderDetailsList.add(orderDetails);
		}
		orderDetailsRepository.saveAll(orderDetailsList);
				PaymentRequest paymentRequest = new PaymentRequest(orderAmount, orderRequest.getAccountNumber(),
				systemAccountNumber);
		ResponseEntity<?> bankTransferResponse = bankAppClient.bankTransfer(paymentRequest);

		if (bankTransferResponse.getStatusCodeValue() <= 201) {
			customerOrder.setOrderStatus(1);
			orderRepository.save(customerOrder);
		}
		return new ResponseEntity<>(
				"Order Created successfully " + customerOrder.getOrderNumber(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> orderHistory(final Long customerId) {
		Optional<Customer> customer = customerRepository.findById(customerId);
		if (!customer.isPresent()) {
			return new ResponseEntity<>("Invalid Customer", HttpStatus.BAD_REQUEST);
		} else {
			OrderHistory orderHistoryResponse = new OrderHistory();
			orderHistoryResponse.setCustomerId(customerId);
			orderHistoryResponse.setCustomerName(customer.get().getCustomerName());
			Set<Order> customerOrders = customer.get().getCustomerOrders().stream()
					.sorted(Comparator.comparing(Order::getOrderDate)).collect(Collectors.toSet());
			List<OrderDto> orders = new ArrayList<>();
			for (Order order : customerOrders) {
				List<ProductOrderDetails> orderProductDetails = new ArrayList<>();
				for (OrderDetails orderDetail : order.getOrderDetails()) {
					ProductEntity product = orderDetail.getProduct();
					orderProductDetails.add(new ProductOrderDetails(product.getProductName(),
							orderDetail.getProductPrice(), orderDetail.getProductQuantity()));
				}
				orders.add(new OrderDto(order.getOrderId(), order.getOrderNumber(), order.getOrderAmount(),
						order.getOrderDate(), orderProductDetails));
			}
			orderHistoryResponse.setOrders(orders);
			return new ResponseEntity<>(orderHistoryResponse, HttpStatus.OK);
		}
	}

}
