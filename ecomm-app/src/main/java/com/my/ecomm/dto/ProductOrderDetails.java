package com.my.ecomm.dto;

import java.io.Serializable;

public class ProductOrderDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String productName;

	private Double productPrice;

	private Integer productQuntity;

	public ProductOrderDetails(String productName, Double productPrice, Integer productQuntity) {
		super();
		this.productName = productName;
		this.productPrice = productPrice;
		this.productQuntity = productQuntity;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	public Integer getProductQuntity() {
		return productQuntity;
	}

	public void setProductQuntity(Integer productQuntity) {
		this.productQuntity = productQuntity;
	}

}
