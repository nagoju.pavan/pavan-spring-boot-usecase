package com.my.ecomm.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ProductItems implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Invalid Product")
	private Long productId;

	@NotNull(message = "Invalid Quatity")
	@Min(message = "Invalid Quatity", value = 1)
	private Integer quantity;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
