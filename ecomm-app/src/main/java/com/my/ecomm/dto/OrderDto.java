package com.my.ecomm.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String orderNumber;

	private Double orderAmount;

	private Date orderDate;

	private List<ProductOrderDetails> orderProductDetails = new ArrayList<>();

	public OrderDto(Long id, String orderNumber, Double orderAmount, Date orderDate,
			List<ProductOrderDetails> orderProductDetails) {
		super();
		this.id = id;
		this.orderNumber = orderNumber;
		this.orderAmount = orderAmount;
		this.orderDate = orderDate;
		this.orderProductDetails = orderProductDetails;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Double getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(Double orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public List<ProductOrderDetails> getOrderProductDetails() {
		return orderProductDetails;
	}

	public void setOrderProductDetails(List<ProductOrderDetails> orderProductDetails) {
		this.orderProductDetails = orderProductDetails;
	}

}
