package com.my.ecomm.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PurchaseOrderRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Invalid Customer")
	private Long customerId;

	@NotNull(message = "Invalid Account Number")
	private String accountNumber;

	@NotEmpty(message = "Invalid ProductItems")
	@NotNull(message = "Invalid ProductItems")
	private List<ProductItems> productItems = new ArrayList<>();

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public List<ProductItems> getProductItems() {
		return productItems;
	}

	public void setProductItems(List<ProductItems> productItems) {
		this.productItems = productItems;
	}

}
