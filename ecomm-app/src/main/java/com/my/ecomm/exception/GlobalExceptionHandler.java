package com.my.ecomm.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler
	protected ResponseEntity<?> handle(HttpMessageNotReadableException messageNotReadableException) {
		return new ResponseEntity<>("Invalid Format", HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	protected ResponseEntity<?> handle(MethodArgumentNotValidException messageNotReadableException) {
		BindingResult result = messageNotReadableException.getBindingResult();
		List<String> errors = new ArrayList<>();
		result.getAllErrors().stream().forEach(error -> {
			errors.add(error.getDefaultMessage());

		});
		return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	protected ResponseEntity<?> handle(MethodArgumentTypeMismatchException messageNotReadableException) {
		return new ResponseEntity<>("Invalid Format", HttpStatus.BAD_REQUEST);
	}

}