package com.my.banktransferapp.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.my.banktransferapp.enity.Account;
import com.my.banktransferapp.enity.User;
import com.my.banktransferapp.repository.UserRepository;
import com.my.banktransferapp.service.UserRegistrationService;
import com.my.banktransferapp.utility.CustomGeneratorUtil;

@Service

public class UserRegistrationServiceImpl implements UserRegistrationService {

	@Autowired
	UserRepository appUserRepository;

	@Override
	@Transactional
	public ResponseEntity<?> userRegistrationForm(final User user) {
		System.out.println("testing the code........");
		ResponseEntity<?> responseEntity = validateAppuserRegistration(user);
		if (responseEntity.getStatusCodeValue() > 200) {
			return responseEntity;
		}

		Account account = user.getAccount();
		account.setCreatedDate(new Date());
		if (account.getBalance() == null || account.getBalance() == 0.0) {
			account.setBalance(5000.0);
		}
		String accountNo = CustomGeneratorUtil.generateRandomAccountNumber();
		account.setAccountNumber(accountNo);
		account.setUser(user);
		appUserRepository.save(user);// User creation is successful
		return new ResponseEntity<>("Customer account : " + accountNo + " created successfully ", HttpStatus.OK);
	}

	private ResponseEntity<?> validateAppuserRegistration(User appUser) {
		if (appUser == null) {
			return new ResponseEntity<>(" Invalid Details !!", HttpStatus.BAD_REQUEST);
		}
		Account account = appUser.getAccount();
		if (account == null) {
			return new ResponseEntity<>("Invalid Account details... ", HttpStatus.BAD_REQUEST);
		}
		if (account.getBalance() == null || account.getBalance() < 0.0) {
			return new ResponseEntity<>(" Invalid Balance... ", HttpStatus.BAD_REQUEST);
		}
		List<User> appUsers = appUserRepository.findByEmailAddress(appUser.getEmailAddress());
		if (!appUsers.isEmpty()) {
			return new ResponseEntity<>(" User Found with Same email address... ", HttpStatus.BAD_REQUEST);
		}
		appUsers = appUserRepository.findByAadharCard(appUser.getAadharCard());
		if (!appUsers.isEmpty()) {
			return new ResponseEntity<>("User Found with Same Aadhar card", HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
