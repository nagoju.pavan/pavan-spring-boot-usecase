package com.my.banktransferapp.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.my.banktransferapp.dto.PaymentRequest;
import com.my.banktransferapp.dto.PaymentResponse;
import com.my.banktransferapp.dto.StatmentReport;
import com.my.banktransferapp.dto.UserStatment;
import com.my.banktransferapp.enity.Account;
import com.my.banktransferapp.enity.Transaction;
import com.my.banktransferapp.repository.AccountRepository;
import com.my.banktransferapp.repository.TransactionRepository;
import com.my.banktransferapp.repository.UserRepository;
import com.my.banktransferapp.service.TranasctionService;
import com.my.banktransferapp.utility.CustomGeneratorUtil;

@Service

public class TransactionServiceImpl implements TranasctionService {

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	UserRepository appUserRepository;

	@Autowired
	AccountRepository accountRepository;

	@Override
	public ResponseEntity<?> statementReport(final Integer month, final Integer year, final String accountNumber) {
		Account account = accountRepository.findByAccountNumber(accountNumber);
		if (accountNumber == null) {
			return new ResponseEntity<>("Account Not Found", HttpStatus.BAD_REQUEST);
		}
		if (month == null || month > 11 || month < 0) {
			return new ResponseEntity<>("Invalid Month...", HttpStatus.BAD_REQUEST);
		}
		List<Transaction> transactions = transactionRepository
				.findByFromAccountAccountNumberOrToAccountAccountNumber(accountNumber, accountNumber);
		StatmentReport statementReport = generateStatementSummaryReport(month, year, accountNumber, account,
				transactions);
		return new ResponseEntity<>(statementReport, HttpStatus.OK);
	}

	@Override
	@Transactional
	public ResponseEntity<?> fundTransfer(final PaymentRequest payment) {
		if (payment.getPayAmount() > 0.0) {
			Transaction tranasction = new Transaction();
			tranasction.setTransactionDate(new Date());
			Account fromAccount = accountRepository.findByAccountNumber(payment.getFromAccount());
			Account toAccount = accountRepository.findByAccountNumber(payment.getToAccount());
			if (fromAccount == null || toAccount == null) {
				return new ResponseEntity<>("Payment failed !! Account Not Found", HttpStatus.BAD_REQUEST);
			}
			if (fromAccount.getBalance() < payment.getPayAmount()) {
				return new ResponseEntity<>("Payment failed !! Insufficient balance ", HttpStatus.BAD_REQUEST);
			}
			tranasction.setFromAccount(fromAccount);
			tranasction.setToAccount(toAccount);
			tranasction.setTransferedAmount(payment.getPayAmount());
			String transactionNo = CustomGeneratorUtil.generateTransactionRefNumber();
			tranasction.setTransactionNo(transactionNo);
			double fromAccountBalance = fromAccount.getBalance() - payment.getPayAmount();
			double toAccountBalance = toAccount.getBalance() + payment.getPayAmount();
			tranasction.setFromAccountBalance(fromAccountBalance);
			tranasction.setToAccountBalance(toAccountBalance);
			transactionRepository.save(tranasction);
			fromAccount.setBalance(fromAccountBalance);
			toAccount.setBalance(toAccountBalance);
			accountRepository.save(fromAccount);
			accountRepository.save(toAccount);
			PaymentResponse paymentResponse = new PaymentResponse(transactionNo, payment.getPayAmount());
			return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(" Payment failed !!", HttpStatus.BAD_REQUEST);
		}
	}

	private StatmentReport generateStatementSummaryReport(final Integer month, final Integer year,
			final String accountNumber, Account account, List<Transaction> transactions) {
		StatmentReport statementReport = new StatmentReport();
		statementReport.setBalance(account.getBalance());
		statementReport.setAccountNumber(accountNumber);
		statementReport.setAccountCreatedDate(account.getCreatedDate());
		List<UserStatment> transactionStatment = new ArrayList<>();
		for (Transaction transaction : transactions) {
			Date txnDate = transaction.getTransactionDate();
			Calendar requestCal = Calendar.getInstance();
			requestCal.setTime(txnDate);
			if (requestCal.get(Calendar.YEAR) == year && requestCal.get(Calendar.MONTH) == month) {
				UserStatment statementInfo = new UserStatment();
				if (transaction.getToAccount().getAccountNumber().equals(accountNumber)) {
					statementInfo.setCreditedAmount(transaction.getTransferedAmount());
					statementInfo.setBalance(transaction.getToAccountBalance());
				} else if (transaction.getFromAccount().getAccountNumber().equals(accountNumber)) {
					statementInfo.setDebitedAmount(transaction.getTransferedAmount());
					statementInfo.setBalance(transaction.getFromAccountBalance());
				}
				statementInfo.setTransactionNo(transaction.getTransactionNo());
				statementInfo.setTransactionDate(transaction.getTransactionDate().toString());
				transactionStatment.add(statementInfo);
			}
		}
		statementReport.setTransactionHistory(transactionStatment);
		return statementReport;
	}

}
