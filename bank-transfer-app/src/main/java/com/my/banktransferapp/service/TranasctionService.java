package com.my.banktransferapp.service;

import org.springframework.http.ResponseEntity;

import com.my.banktransferapp.dto.PaymentRequest;

public interface TranasctionService {

	ResponseEntity<?> statementReport(final Integer month, final Integer year, final String accountNumber);

	ResponseEntity<?> fundTransfer(final PaymentRequest payment);

}
