package com.my.banktransferapp.service;

import org.springframework.http.ResponseEntity;

import com.my.banktransferapp.enity.User;

public interface UserRegistrationService {

	ResponseEntity<?> userRegistrationForm(final User user);

}
