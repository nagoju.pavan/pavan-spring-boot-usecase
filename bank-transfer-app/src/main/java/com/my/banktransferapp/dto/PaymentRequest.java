package com.my.banktransferapp.dto;

import java.io.Serializable;

public class PaymentRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fromAccount;

	private String toAccount;

	private Double payAmount;

	public String getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public String getToAccount() {
		return toAccount;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public Double getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(Double payAmount) {
		this.payAmount = payAmount;
	}

}
