package com.my.banktransferapp.dto;

import java.io.Serializable;

public class PaymentResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String transactionNo;

	private Double paidAmount;

	public PaymentResponse(String transactionNo, Double paidAmount) {
		super();
		this.transactionNo = transactionNo;
		this.paidAmount = paidAmount;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

}
