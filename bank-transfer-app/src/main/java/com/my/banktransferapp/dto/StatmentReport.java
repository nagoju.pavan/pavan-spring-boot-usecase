package com.my.banktransferapp.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StatmentReport implements Serializable {

	private static final long serialVersionUID = 1L;

	private String accountNumber;

	private String generatedDate;

	private Double balance;

	private Date accountCreatedDate;

	private List<UserStatment> transactionHistory = new ArrayList<>();

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getGeneratedDate() {
		return generatedDate;
	}

	public void setGeneratedDate(String generatedDate) {
		this.generatedDate = generatedDate;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Date getAccountCreatedDate() {
		return accountCreatedDate;
	}

	public void setAccountCreatedDate(Date accountCreatedDate) {
		this.accountCreatedDate = accountCreatedDate;
	}

	public List<UserStatment> getTransactionHistory() {
		return transactionHistory;
	}

	public void setTransactionHistory(List<UserStatment> transactionHistory) {
		this.transactionHistory = transactionHistory;
	}

}
