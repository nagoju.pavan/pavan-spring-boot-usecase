package com.my.banktransferapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.banktransferapp.enity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	List<Transaction> findByFromAccountAccountNumberOrToAccountAccountNumber(String fromAccount, String toAccount);

}
