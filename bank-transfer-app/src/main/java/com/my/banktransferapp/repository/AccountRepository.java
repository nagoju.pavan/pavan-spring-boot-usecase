package com.my.banktransferapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.banktransferapp.enity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {

	Account findByAccountNumber(String accountNumber);

}
