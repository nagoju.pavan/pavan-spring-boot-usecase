package com.my.banktransferapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.banktransferapp.enity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	List<User> findByEmailAddress(String emailAddress);

	List<User> findByAadharCard(String aadharCard);

}
