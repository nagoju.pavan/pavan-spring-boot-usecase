package com.my.banktransferapp.utility;

import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;

public class CustomGeneratorUtil {

	public static String generateRandomAccountNumber() {
		String accountNumber = RandomStringUtils.randomNumeric(9, 18).toString();
		return accountNumber;
	}

	public static String generateTransactionRefNumber() {

		return UUID.randomUUID().toString();
	}

}
