package com.my.banktransferapp.exception;

import java.util.ArrayList;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler
	public ResponseEntity<?> handle(ConstraintViolationException exception) {
		String errorMessage = new ArrayList<>(exception.getConstraintViolations()).get(0).getMessage();
		return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	public ResponseEntity<?> handle(HttpMessageNotReadableException messageNotReadableException) {
		return new ResponseEntity<>("Invalid Request", HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	public ResponseEntity<?> hadle(HttpMediaTypeNotSupportedException httpMediaTypeNotSupportedException) {
		return new ResponseEntity<>("Formate Not Supported", HttpStatus.BAD_REQUEST);
	}
}
