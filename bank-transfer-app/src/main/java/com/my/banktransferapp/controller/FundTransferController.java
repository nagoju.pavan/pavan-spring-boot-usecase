package com.my.banktransferapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.my.banktransferapp.dto.PaymentRequest;
import com.my.banktransferapp.service.TranasctionService;

@RestController
@RequestMapping("/bank")
public class FundTransferController {

	@Autowired
	TranasctionService tranasctionService;

	@PostMapping(path = "/fund-transfer")
	public ResponseEntity<?> processPayment(@RequestBody PaymentRequest payment) {
		ResponseEntity<?> responseEntity = tranasctionService.fundTransfer(payment);
		return responseEntity;
	}

}
