package com.my.banktransferapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.my.banktransferapp.enity.User;
import com.my.banktransferapp.service.UserRegistrationService;

@RestController
@RequestMapping("/bank")

public class UserController {

	@Autowired
	UserRegistrationService userRegistrationService;

	@PostMapping("/user-account/registration")
	public ResponseEntity<?> userAccountRegistration(@Valid @RequestBody User appUser) {
		return userRegistrationService.userRegistrationForm(appUser);
	}

}
