package com.my.banktransferapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.my.banktransferapp.service.TranasctionService;

@RestController
@RequestMapping("/bank")
public class BankUserStatmentController {

	@Autowired
	TranasctionService tranasctionService;

	@GetMapping("/report/transaction")
	public ResponseEntity<?> generateMonthlyTransactionReport(@RequestParam Integer month, @RequestParam Integer year,
			@RequestParam String accountNumber) {
		return tranasctionService.statementReport(month, year, accountNumber);
	}

}
