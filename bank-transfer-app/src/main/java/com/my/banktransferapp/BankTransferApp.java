package com.my.banktransferapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class BankTransferApp {

	public static void main(String[] args) {
		SpringApplication.run(BankTransferApp.class, args);
	}

}
